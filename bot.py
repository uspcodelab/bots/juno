import telebot
import requests
import logging
import os
import re
from bs4 import BeautifulSoup

## JunoWeb - Desvendando o JúpiterWeb

############################################################
"""
Setup the bot with the token given by the BotFather
obs: the token is stored in an evironment variable
"""
token = "593784215:AAExTXpT5Kcr2D-Gyp7RYN6_Fzwv9dR7p0Q"

bot = telebot.TeleBot(token)
logger = telebot.logger
telebot.logger.setLevel(logging.DEBUG)

@bot.message_handler(commands=['start','help'])
def welcome(message):
    bot.reply_to(message,"Bem vindo!")


def sem_codigo(words):
    return (len(words) < 2)


def extrai_codigo(msg):
    words = msg.split()
    if sem_codigo(words):
        return None
    return words[1].upper()


def dis_url(code):
    return "https://uspdigital.usp.br/jupiterweb/obterDisciplina?sgldis={code}&nomdis=".format(code=code)

def dis_req_url(code):
    return "https://uspdigital.usp.br/jupiterweb/listarCursosRequisitos?coddis={}".format(code)


def create_soup(code, url_generator):
    url = url_generator(code)
    html = requests.get(url).text
    return BeautifulSoup(html)


@bot.message_handler(commands=['url_dis'])
def give_url(message):
    code = extrai_codigo(message.text)
    if code is None:
        bot.reply_to(message, "Favor informar o código da disciplina")
    else:
        # como verificar validade do code?
        url = dis_url(code)
        bot.reply_to(message, url)


@bot.message_handler(commands=['nome_dis'])
def fetch_discipline(message):
    code = extrai_codigo(message.text)
    if code is None:
        bot.reply_to(message, "Favor informar o código da disciplina")
    else:
        soup = create_soup(code, dis_url)
        results = soup.find_all('span', "txt_arial_10pt_black")
        if len(results) < 3:
            bot.reply_to(message, "Sinto muito, não encontrei {code}".format(code=code))
        else:
            bot.reply_to(message, results[2].text)



def limpa(texto):
    texto = re.sub(r"\n","", texto)
    texto = re.sub(r"Curso: [0-9]{5}","Curso: ", texto)
    texto = re.sub(r"- Período ideal: [0-9]","", texto)
    texto = re.sub(r"\ Requisito","", texto)
    texto = re.sub(r"\(noturno\)",u"\U0001F31A", texto)
    texto = re.sub(r"\(diurno\)",u"\U0001F31D", texto)
    return re.sub(r"\ {2,}", "", texto)


def stringuifica(results):
    s = ''
    title = {'now': True, 'prev': False}
    for r in results:
        if re.match("^Curso",r):
            title['now'], title['prev'] = True, title['now']
            if not title['prev']:
                s += "\n\n"
            s += "{}\n".format(r)
        else:
            title['now'], title['prev'] = False, title['now']
            if re.match(r"(\d{7})|([A-Z]{3}\d{4})", r):
                s += u"\U00002757 {}\n".format(r)
            else:
                s += "{}\n".format(r)

    return s


@bot.message_handler(commands=['req_dis'])
def list_requisito(message):
    code = extrai_codigo(message.text)
    if code is None:
        bot.reply_to(message, "Favor informar o código da disciplina")
    else:
        soup = create_soup(code, dis_req_url)
        raw = soup.find_all('tr', 'txt_verdana_8pt_gray')
        if len(raw) > 0:
            results = [ limpa(el.text) for el in raw ]
            s = stringuifica(results)
            bot.reply_to(message, s)
        else:
            bot.reply_to(message, u"\U0001F64F {} não tem pré-requisitos".format(code))



@bot.message_handler(func=lambda m: True)
def missed(message):
    bot.send_message(message.chat.id, "Sinto muito, não reconheco '{}'".format(message.text))


bot.polling(none_stop=True)